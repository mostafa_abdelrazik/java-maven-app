FROM maven:3.8.2-jdk-11

COPY ./target/java-maven-app-1.1.0.jar .

CMD ["java", "-jar", "java-maven-app-1.1.0.jar"]
